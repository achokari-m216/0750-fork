# 0750 Exercice Git avancé : Fork, Branche, Script Python et Merge Request

## Objectifs

* Appliquer les concepts de fork, branche et merge request sur GitLab.
* Compléter un script Python en s'aidant du README.
* Soumettre une merge request pour validation.

## Ressources :

* Documentation GitLab : [https://docs.gitlab.com/](https://docs.gitlab.com/)

## Instructions

**1. Forker le projet**

Rendez-vous sur le dépôt GitLab du projet et cliquez sur le bouton "Fork". Cela créera une copie du projet dans votre espace de travail GitLab.

**2. Créer une nouvelle branche**

Exécutez les commandes suivantes :

```
git checkout -b <votre_identifiant>
```

Remplacez `<votre_identifiant>` par les 4 premières lettres de votre nom et les 4 premières lettres de votre prénom.

**3. Compléter le README**

Modifiez le fichier README.md pour chaque étape que vous effectuez. Indiquez les commandes que vous utilisez et les résultats obtenus dans les balises suivantes:
```
# TODO saississez les commandes utilisées dans les balises suivantes
```

**4. Compléter le script Python**

Complétez le script `send.py` comme demandé dans p0500. Assurez-vous de tester votre code avant de le commiter.

**5. Pusher vos modifications**

```
# TODO indiquez les commandes utilisées
```

**6. Créer une merge request**

Sur GitLab, allez sur votre fork du projet et cliquez sur "Merge Requests". Cliquez sur "New Merge Request". Sélectionnez la branche `master` comme base et votre branche comme source. Ajoutez un titre et une description clairs à votre merge request.

**7. Attendre la validation**

Patientez que la merge request soit validée par le professeur. Si nécessaire, apportez les modifications demandées.


## Conseils :

* Lisez attentivement le README avant de commencer.
* N'hésitez pas à poser des questions au professeur si vous avez besoin d'aide.
* Prenez le temps de tester votre code avant de le commiter.
* Rédigez une merge request claire et concise.
